# Faktura

This is desktop program, written using a SWT library in Java with SQLite database engine, to which communication is provided via JDBC interface.
It generates invoices based on information stored in the database.

## Download
You can download the program [here](http://env.ct8.pl/invoice/public/Invoice.zip).

## Requirements
Java >= 11.0.2

## Demo
Program after start
![Program after start](http://env.ct8.pl/invoice/public/start.png)
Companies list
![Companies list](http://env.ct8.pl/invoice/public/podmioty.png)
Editing the company
![Editing the company](http://env.ct8.pl/invoice/public/edycja.png)
Completed form
![Completed form](http://env.ct8.pl/invoice/public/gotowa.png)