package InvoiceCreator.Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public class Company extends Model {
    private int id;
    private String name, street, city, zip, bank_account;
    private long tax_id;

    public int insert() {
        try {
            PreparedStatement statement = conn.prepareStatement("INSERT INTO companies (name, street, city, zip, tax_id, bank_account) VALUES (?, ?, ?, ?, ?, ?)");
            statement.setString(1, name);
            statement.setString(2, street);
            statement.setString(3, city);
            statement.setString(4, zip);
            statement.setLong(5, tax_id);
            statement.setString(6, bank_account);
            statement.execute();
            this.id = statement.getGeneratedKeys().getInt(1);
        } catch (Exception e) {
            e.printStackTrace();
            this.id = -1;
        }
        return this.id;
    }

    public boolean update() {
        try {
            PreparedStatement statement = conn.prepareStatement("UPDATE companies SET name=?, street=?, city=?, zip=?, tax_id=?, bank_account=? WHERE id=?");
            statement.setString(1, getName());
            statement.setString(2, getStreet());
            statement.setString(3, getCity());
            statement.setString(4, getZip());
            statement.setLong(5, getTax_id());
            statement.setString(6, getBank_account());
            statement.setInt(7, getId());
            return statement.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete() {
        return delete(getId());
    }

    private static boolean delete(int id) {
        try {
            PreparedStatement statement = conn.prepareStatement("DELETE FROM companies WHERE id = ?");
            statement.setInt(1, id);
            return statement.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static List<Company> all() {
        return search("");
    }

    public static List<Company> search(String name) {
        List<Company> companies = new LinkedList<>();
        try {
            PreparedStatement statement;
            if(name.equals("")) {
                statement = conn.prepareStatement("SELECT * FROM companies ORDER BY name");
            } else {
                statement = conn.prepareStatement("SELECT * FROM companies WHERE name like ? ORDER BY name");
                statement.setString(1, "%" + name.replaceAll("[^\\x00-\\x7F]", "_") + "%");
            }
            ResultSet result = statement.executeQuery();
            while ( result.next() ) {
                companies.add(new Company()
                        .setId(result.getInt("id"))
                        .setName(result.getString("name"))
                        .setStreet(result.getString("street"))
                        .setCity(result.getString("city"))
                        .setZip(result.getString("zip"))
                        .setTax_id(result.getLong("tax_id"))
                        .setBank_account(result.getString("bank_account")));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return companies;
    }

    public static Company get(String name) {
        try {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM companies WHERE name = ?");
            statement.setString(1, name);
            ResultSet result = statement.executeQuery();
            return new Company()
                .setId(result.getInt("id"))
                .setName(result.getString("name"))
                .setStreet(result.getString("street"))
                .setCity(result.getString("city"))
                .setZip(result.getString("zip"))
                .setTax_id(result.getLong("tax_id"))
                .setBank_account(result.getString("bank_account"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static Company get(int id) {
        try {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM companies WHERE id = ?");
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            return new Company()
                    .setId(result.getInt("id"))
                    .setName(result.getString("name"))
                    .setStreet(result.getString("street"))
                    .setCity(result.getString("city"))
                    .setZip(result.getString("zip"))
                    .setTax_id(result.getLong("tax_id"))
                    .setBank_account(result.getString("bank_account"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    int getId() {
        return id;
    }

    private Company setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Company setName(String name) {
        this.name = name;
        return this;
    }

    public String getStreet() {
        return street;
    }

    public Company setStreet(String street) {
        this.street = street;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Company setCity(String city) {
        this.city = city;
        return this;
    }

    public String getZip() {
        return zip;
    }

    public Company setZip(String zip) {
        this.zip = zip;
        return this;
    }

    public String getBank_account() {
        return bank_account;
    }

    public Company setBank_account(String bank_account) {
        this.bank_account = bank_account;
        return this;
    }

    public long getTax_id() {
        return tax_id;
    }

    public Company setTax_id(long tax_id) {
        this.tax_id = tax_id;
        return this;
    }
}