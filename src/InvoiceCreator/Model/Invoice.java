package InvoiceCreator.Model;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.*;

public class Invoice extends Model {

    private int id;
    private String number;
    private String currency;
    private LocalDate date;
    private LocalDate payment_date;
    private Company buyer;
    private Company seller;
    private LinkedHashMap<Product, Integer> products = new LinkedHashMap<>();

    boolean get() {
        try {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM invoices WHERE id = ?");
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            number = rs.getString("number");
            currency = rs.getString("currency");
            date = rs.getDate("date").toLocalDate();
            payment_date = rs.getDate("payment_date").toLocalDate();
            buyer = Company.get(rs.getInt("buyer_id"));
            seller = Company.get(rs.getInt("seller_id"));
            loadProducts();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void loadProducts() {
        try {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM invoices_products WHERE invoice_id = ?");
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next())
                products.put(new Product().setId(rs.getInt("product_id")).get(), rs.getInt("quantity"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int insert() {
        try {
            PreparedStatement statement = conn.prepareStatement("INSERT INTO invoices (number, buyer_id, seller_id, date, payment_date, currency) VALUES (?, ?, ?, ?, ?, ?)");
            statement.setString(1, number);
            statement.setInt(2, buyer.getId());
            statement.setInt(3, seller.getId());
            statement.setDate(4, Date.valueOf(date));
            statement.setDate(5, Date.valueOf(payment_date));
            statement.setString(6, currency);
            statement.execute();
            this.id = statement.getGeneratedKeys().getInt(1);
            insertProducts();
            return this.id;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    private void insertProducts() {
        StringJoiner params = new StringJoiner(",");
        for(int i=0; i<products.size(); i++)
            params.add("(?,?,?)");
        try {
            PreparedStatement statement = conn.prepareStatement("INSERT INTO invoices_products (invoice_id, product_id, quantity) VALUES "+params.toString());
            int i = 1;
            for(Map.Entry<Product, Integer> entry : products.entrySet()) {
                statement.setInt(i++, this.id);
                statement.setInt(i++, entry.getKey().getId());
                statement.setInt(i++, entry.getValue());
            }
            statement.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public boolean delete() {
//        return delete(id);
//    }

    static boolean delete(int id) {
        try {
            deleteProducts(id);
            PreparedStatement statement = conn.prepareStatement("DELETE FROM invoices WHERE id = ?");
            statement.setInt(1, id);
            return statement.executeUpdate() > 0;
        } catch (Exception e) {
            return false;
        }
    }

    private static void deleteProducts(int id) {
        try {
            PreparedStatement statement = conn.prepareStatement("DELETE FROM invoices_products WHERE invoice_id = ?");
            statement.setInt(1, id);
            statement.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public int getId() {
//        return id;
//    }

    Invoice setId(int id) {
        this.id = id;
        return this;
    }

    public String getNumber() {
        return number;
    }

    public Invoice setNumber(String number) {
        this.number = number;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public Invoice setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public LocalDate getDate() {
        return date;
    }

    public Invoice setDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public LocalDate getPayment_date() {
        return payment_date;
    }

    public Invoice setPayment_date(LocalDate payment_date) {
        this.payment_date = payment_date;
        return this;
    }

    public Company getBuyer() {
        return buyer;
    }

    public Invoice setBuyer(Company buyer) {
        this.buyer = buyer;
        return this;
    }

    public Company getSeller() {
        return seller;
    }

    public Invoice setSeller(Company seller) {
        this.seller = seller;
        return this;
    }

    public void addProduct(Product product, int quantity) {
            products.put(product, quantity);
    }

//    public static boolean addProduct(Product product, int quantity) {
//        try {
//            productList.put(product, quantity);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//        return true;
//    }

//    public boolean deleteProduct(Product product) {
//        try {
//            products.remove(product);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//        return true;
//    }
//
//    public static boolean deleteProduct(Product product) {
//        try {
//            productList.remove(product);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//        return true;
//    }

    public LinkedHashMap<Product, Integer> getProducts() {
        return products;
    }
//
//    public void setProducts(LinkedHashMap<Product, Integer> products) {
//        this.products = products;
//    }
//
//    public LinkedHashMap<Product, Integer> getProductList() {
//        return productList;
//    }
//
//    public void setProductList(LinkedHashMap<Product, Integer> productList) {
//        this.productList = productList;
//    }

//    public List<Product> getProducts() {
//        return products;
//    }
//
//    public Invoice setProducts(List<Product> products) {
//        this.products = products;
//        return this;
//    }
}
