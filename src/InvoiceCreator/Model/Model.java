package InvoiceCreator.Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

abstract class Model {

    static Connection conn;

    static {
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:db.db");
            Statement init = conn.createStatement();
            init.execute("PRAGMA foreign_keys=ON");
            init.execute("CREATE TABLE IF NOT EXISTS products (id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(255) NOT NULL, price real NOT NULL, tax real NOT NULL);");
            init.execute("CREATE TABLE IF NOT EXISTS \"companies\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT, \"name\" varchar(255) NOT NULL UNIQUE, \"street\" varchar(255) NOT NULL, \"zip\" varchar(6) NOT NULL, \"tax_id\" int NOT NULL, \"bank_account\" varchar(26) NOT NULL, \"city\" varchar(255) NOT NULL);");
            init.execute("CREATE TABLE IF NOT EXISTS \"invoices\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT, \"number\" varchar(255) NOT NULL UNIQUE, \"buyer_id\" INTEGER NOT NULL, \"seller_id\" INTEGER NOT NULL, \"date\" date NOT NULL, \"payment_date\" INTEGER, \"currency\" varchar(3) NOT NULL, FOREIGN KEY (buyer_id) REFERENCES companies(id) ON DELETE CASCADE, FOREIGN KEY (seller_id) REFERENCES companies(id) ON DELETE CASCADE);");
            init.execute("CREATE TABLE IF NOT EXISTS invoices_products(invoice_id integer, product_id integer, quantity integer, FOREIGN KEY (invoice_id) REFERENCES invoices(id) ON DELETE CASCADE, FOREIGN KEY (product_id) REFERENCES products(id) ON DELETE CASCADE);");
        } catch (SQLException e) {
            e.printStackTrace();
            conn = null;
        }
    }

}
