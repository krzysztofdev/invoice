package InvoiceCreator.Model;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.LinkedList;

public class BriefInvoice extends Model{
    private int id;
    private LocalDate date;
    private String number;
    private String seller;
    private String buyer;

    public boolean delete() {
        return Invoice.delete(id);
    }

    public static LinkedList<BriefInvoice> list() {
        LinkedList<BriefInvoice> list = new LinkedList<> ();
        try {
            ResultSet result = conn.createStatement().executeQuery("SELECT i.id, i.date, i.number, b.name buyer, s.name seller FROM invoices i LEFT JOIN companies b ON i.buyer_id = b.id LEFT JOIN companies s ON i.seller_id = s.id ORDER BY i.date, i.id DESC");
            while (result.next())
                list.add(new BriefInvoice()
                        .setId(result.getInt("id"))
                        .setDate(result.getDate("date").toLocalDate())
                        .setNumber(result.getString("number"))
                        .setSeller(result.getString("seller"))
                        .setBuyer(result.getString("buyer"))
                );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Invoice get() {
        Invoice invoice = new Invoice().setId(id);
        return invoice.get() ? invoice : null;
    }

    private BriefInvoice setId(int id) {
        this.id = id;
        return this;
    }

    public String getNumber() {
        return number;
    }

    private BriefInvoice setNumber(String number) {
        this.number = number;
        return this;
    }

    public String getSeller() {
        return seller;
    }

    private BriefInvoice setSeller(String seller) {
        this.seller = seller;
        return this;
    }

    public String getBuyer() {
        return buyer;
    }

    private BriefInvoice setBuyer(String buyer) {
        this.buyer = buyer;
        return this;
    }

    public LocalDate getDate() {
        return date;
    }

    private BriefInvoice setDate(LocalDate date) {
        this.date = date;
        return this;
    }
}
