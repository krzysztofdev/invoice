package InvoiceCreator.Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

public class Product extends Model {

    private int id;
    private String name;
    private double price;
    private double tax;

//    private Product(int id, String name, double price, double tax) {
//        this.setId(id);
//        this.setName(name);
//        this.setPrice(price);
//        this.setTax(tax);
//    }

    public int insert() {
        try {
            PreparedStatement statement = conn.prepareStatement("INSERT INTO products (name, price, tax) VALUES (?, ?, ?)");
            statement.setString(1, name);
            statement.setDouble(2, price);
            statement.setDouble(3, tax);
            statement.execute();
            return statement.getGeneratedKeys().getInt(1);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public boolean update() {
        try {
            PreparedStatement statement = conn.prepareStatement("UPDATE products SET name=?, price=?, tax=? WHERE id=?");
            statement.setString(1, name);
            statement.setDouble(2, price);
            statement.setDouble(3, tax);
            statement.setInt(4, id);
            return statement.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete() {
        try {
            PreparedStatement statement = conn.prepareStatement("DELETE FROM products WHERE id = ?");
            statement.setInt(1, id);
            return statement.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<Product> all() {
        return search("");
    }

    public List<Product> search(String name) {
        List<Product> products = new LinkedList<>();
        try {
            PreparedStatement statement;
            if(name.equals("")) {
                statement = conn.prepareStatement("SELECT * FROM products ORDER BY name");
            } else {
                statement = conn.prepareStatement("SELECT * FROM products WHERE name like ? ORDER BY name");
                statement.setString(1, "%" + name.replaceAll("[^\\x00-\\x7F]", "_") + "%");
            }
            ResultSet result = statement.executeQuery();
            while ( result.next() )
                products.add(new Product()
                        .setId(result.getInt("id"))
                        .setName(result.getString("name"))
                        .setPrice(result.getDouble("price"))
                        .setTax(result.getDouble("tax")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return products;
    }

    Product get() {
        try {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM products WHERE id=?");
            statement.setInt(1, getId());
            ResultSet result = statement.executeQuery();
            this.setId(result.getInt("id"));
            this.setName(result.getString("name"));
            this.setPrice(result.getDouble("price"));
            this.setTax(result.getDouble("tax"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

//    public static Product get(int id) {
//        Product product = new Product();
//        product.setId(id);
//        return product.get();
//    }

    public Product get(String name) {
        try {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM products WHERE name=?");
            statement.setString(1, name);
            ResultSet result = statement.executeQuery();
            this.setId(result.getInt("id"));
            this.setName(result.getString("name"));
            this.setPrice(result.getDouble("price"));
            this.setTax(result.getDouble("tax"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

    int getId() {
        return id;
    }

    Product setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public Product setPrice(double price) {
        this.price = price;
        return this;
    }

    public double getTax() {
        return tax;
    }

    public Product setTax(double tax) {
        this.tax = tax;
        return this;
    }
}