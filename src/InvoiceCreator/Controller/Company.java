package InvoiceCreator.Controller;

import java.util.List;

public class Company {

    public static InvoiceCreator.Model.Company company(String name) {
        return InvoiceCreator.Model.Company.get(name);
    }

    public static List<InvoiceCreator.Model.Company> companies(String name) {
        return InvoiceCreator.Model.Company.search(name);
    }

    public static List<InvoiceCreator.Model.Company> companies() {
        return InvoiceCreator.Model.Company.all();
    }

}
