package InvoiceCreator.Controller;

import InvoiceCreator.View.Invoice.Generator;

public class Invoice {

    public static void main(String[] args) {
        new InvoiceCreator.View.Invoice.Invoice().open();
    }

    public static void generate(InvoiceCreator.Model.Invoice invoice) throws java.io.IOException {
        invoice.insert();
        new Generator(invoice).createInvoice();
    }
}