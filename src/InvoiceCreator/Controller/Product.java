package InvoiceCreator.Controller;

import java.util.List;

public class Product {

    public static List<InvoiceCreator.Model.Product> products(String name) {
        return new InvoiceCreator.Model.Product().search(name);
    }

    public static List<InvoiceCreator.Model.Product> products() {
        return new InvoiceCreator.Model.Product().all();
    }

    public static InvoiceCreator.Model.Product product(String name) {
        return new InvoiceCreator.Model.Product().get(name);
    }

}
