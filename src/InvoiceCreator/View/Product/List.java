package InvoiceCreator.View.Product;

import InvoiceCreator.Controller.Product;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.*;

public class List implements Manage.ActionCallback {

    private Shell parent;
    private Table table;

    public List(Shell parent) {
        this.parent = parent;
    }

    public void open() {
        Shell shell = new Shell(parent);
        shell.setLayout(new GridLayout(1, true));
        shell.setText("Produkty");
        shell.setSize(640, 480);

        table = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        for (String title : new String[]{ "Nazwa towaru/usługi", "Cena jednost.", "VAT %"})
            new TableColumn(table, SWT.NONE).setText(title);

        for (InvoiceCreator.Model.Product product: Product.products())
            fillTableItem(product, new TableItem(table, SWT.NONE));

        refreshTableColumns();

        Composite horizontal = new Composite(shell, SWT.NONE);
        horizontal.setLayout(new RowLayout());
        Button add = new Button(horizontal, SWT.NONE);
        add.setText("Nowy produkt");
        Button delete = new Button(horizontal, SWT.NONE);
        delete.setText("Usuń produkt");
        Button edit = new Button(horizontal, SWT.NONE);
        edit.setText("Edytuj produkt");

        add.addListener(SWT.Selection, event -> new Manage(shell, List.this).open());

        delete.addListener(SWT.Selection, event -> {
            int selected = table.getSelectionIndex();
            if(selected>=0) {
                TableItem item = table.getItem(selected);
                if(((InvoiceCreator.Model.Product) item.getData()).delete())
                    item.dispose();
            }
        });

        edit.addListener(SWT.Selection, event -> new Manage(shell,(InvoiceCreator.Model.Product) table.getItem(table.getSelectionIndex()).getData(), List.this).open());

        shell.open();
    }

    @Override
    public void addItem(InvoiceCreator.Model.Product product) {
        fillTableItem(product, new TableItem(table, SWT.NONE));
        refreshTableColumns();
    }

    @Override
    public void editItem(InvoiceCreator.Model.Product product) {
        fillTableItem(product, table.getItem(table.getSelectionIndex()));
        refreshTableColumns();
    }

    private void refreshTableColumns() {
        for (TableColumn column : table.getColumns())
            column.pack();
    }

    private void fillTableItem(InvoiceCreator.Model.Product product, TableItem item) {
        item.setData(product);
        String[] columns = {product.getName(), String.valueOf(product.getPrice()), String.valueOf(product.getTax())};
        for(int i = 0; i<columns.length; i++) {
            item.setText(i, columns[i]);
        }
    }
}