package InvoiceCreator.View.Product;

import InvoiceCreator.Model.Product;
import InvoiceCreator.View.ErrorMessage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

class Manage {

    private Shell parent;
    private Product data;
    private boolean insert = false;
    private ActionCallback callback;

    Manage(Shell parent, ActionCallback callback) {
        this.parent = parent;
        this.insert = true;
        this.callback = callback;
    }

    Manage(Shell parent, Product data, ActionCallback callback) {
        this.parent = parent;
        this.data = data;
        this.callback = callback;
    }

    public void open() {
        Shell shell = new Shell(parent);
        shell.setLayout(new GridLayout(2, false));
        shell.setSize(320, 150);

        if(insert)
            shell.setText("Dodaj produkt");
        else
            shell.setText("Edytuj produkt");

        GridData fillHorizontal = new GridData(GridData.FILL_HORIZONTAL);

        new Label(shell, SWT.NONE).setText("Nazwa");
        Text name = new Text(shell, SWT.BORDER);
        name.setLayoutData(fillHorizontal);
        new Label(shell, SWT.NONE).setText("Cena");
        Text price = new Text(shell, SWT.BORDER);
        price.setLayoutData(fillHorizontal);
        new Label(shell, SWT.NONE).setText("VAT");
        Text tax = new Text(shell, SWT.BORDER);
        tax.setLayoutData(fillHorizontal);
        Button submit = new Button(shell, SWT.NONE);
        submit.setText("Zapisz");

        if(!insert) {
            name.setText(data.getName());
            price.setText(String.valueOf(data.getPrice()));
            tax.setText(String.valueOf(data.getTax()));
        } else data = new Product();

        submit.addListener(SWT.Selection, event -> {
                data.setName(name.getText())
                        .setPrice(Double.parseDouble(price.getText()))
                        .setTax(Double.parseDouble(tax.getText()));

                if(insert ? data.insert() > 0 : data.update()) {
                    if(insert)
                        callback.addItem(data);
                    else
                        callback.editItem(data);

                    shell.close();
                } else ErrorMessage.open(shell);

        });
        shell.open();
    }

    interface ActionCallback {
        void addItem(Product product);
        void editItem(Product product);
    }
}
