package InvoiceCreator.View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class ErrorMessage {

    public static void open(Shell shell) {
        open(shell, "Wystąpił błąd");
    }

    public static void open(Shell shell, String message) {
        MessageBox dialog = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
        dialog.setText("Błąd");
        dialog.setMessage(message);
        dialog.open();
    }

}
