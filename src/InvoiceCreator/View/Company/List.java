package InvoiceCreator.View.Company;

import InvoiceCreator.Controller.Company;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.*;

public class List implements Manage.ActionCallback {

    private Shell parent;
    private Table table;

    public List(Shell parent) {
        this.parent = parent;
    }

    public void open() {
        Shell shell = new Shell(parent);
        shell.setLayout(new GridLayout(1, false));
        shell.setSize(640, 480);
        shell.setText("Podmioty");

        table = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        for (String title : new String[]{"Nazwa", "Ulica", "Miasto", "Kod pocztowy", "NIP", "Numer konta"})
            new TableColumn(table, SWT.NONE).setText(title);

        for (InvoiceCreator.Model.Company company: Company.companies())
            fillTableItem(company, new TableItem(table, SWT.NONE));

        refreshTableColumns();

        Composite buttons = new Composite(shell, SWT.NONE);
        buttons.setLayout(new RowLayout());
        Button add = new Button(buttons, SWT.NONE);
        add.setText("Nowy podmiot");
        Button delete = new Button(buttons, SWT.NONE);
        delete.setText("Usuń podmiot");
        Button edit = new Button(buttons, SWT.NONE);
        edit.setText("Edytuj podmiot");

        add.addListener(SWT.Selection, event -> new Manage(shell, List.this).open());

        delete.addListener(SWT.Selection, event -> {
            if(table.getSelectionIndex()>=0) {
                TableItem item = table.getItem(table.getSelectionIndex());
                InvoiceCreator.Model.Company company = (InvoiceCreator.Model.Company) item.getData();
                if(company.delete())
                    item.dispose();
            }
        });

        edit.addListener(SWT.Selection, event -> {
//                new Form(Integer.parseInt(table.getSelection()[0].getText())).init(shell, table);
            if(table.getSelectionIndex() >= 0)
                new Manage(shell, (InvoiceCreator.Model.Company) table.getItem(table.getSelectionIndex()).getData(), List.this).open();
        });

        shell.open();
    }


    @Override
    public void addItem(InvoiceCreator.Model.Company company) {
        fillTableItem(company, new TableItem(table, SWT.NONE));
        refreshTableColumns();
    }

    @Override
    public void editItem(InvoiceCreator.Model.Company company) {
        fillTableItem(company, table.getItem(table.getSelectionIndex()));
        refreshTableColumns();
    }

    private void refreshTableColumns() {
        for (TableColumn column : table.getColumns())
            column.pack();
    }

    private void fillTableItem(InvoiceCreator.Model.Company company, TableItem item) {
        item.setData(company);
        String[] columns = {company.getName(), company.getStreet(), company.getCity(), company.getZip(), String.valueOf(company.getTax_id()), company.getBank_account()};
        for(int i = 0; i<columns.length; i++)
            item.setText(i, columns[i]);
    }
}
