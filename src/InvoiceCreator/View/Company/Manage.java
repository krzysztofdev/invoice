package InvoiceCreator.View.Company;

import InvoiceCreator.Model.Company;
import InvoiceCreator.View.ErrorMessage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

class Manage {

    private Shell parent;
    private Company data;
    private boolean insert = false;
    private ActionCallback callback;

    Manage(Shell parent, ActionCallback callback) {
        this.parent = parent;
        this.insert = true;
        this.callback = callback;
    }

    Manage(Shell parent, Company data, ActionCallback callback) {
        this.parent = parent;
        this.data = data;
        this.callback = callback;
    }

    public void open() {
        Shell shell = new Shell(parent);
        shell.setLayout(new GridLayout(2, false));
        shell.setSize(480, 230);

        if(insert)
            shell.setText("Dodaj firmę");
        else
            shell.setText("Edytuj firmę");

        GridData fillHorizontal = new GridData(GridData.FILL_HORIZONTAL);

        new Label(shell, SWT.NONE).setText("Nazwa");
        Text name = new Text(shell, SWT.BORDER);
        name.setLayoutData(fillHorizontal);
        new Label(shell, SWT.NONE).setText("Ulica");
        Text street = new Text(shell, SWT.BORDER);
        street.setLayoutData(fillHorizontal);
        new Label(shell, SWT.NONE).setText("Miejscowość");
        Text city = new Text(shell, SWT.BORDER);
        city.setLayoutData(fillHorizontal);
        new Label(shell, SWT.NONE).setText("Kod pocztowy");
        Text zip = new Text(shell, SWT.BORDER);
        zip.setLayoutData(fillHorizontal);
        new Label(shell, SWT.NONE).setText("NIP");
        Text tax_id = new Text(shell, SWT.BORDER);
        tax_id.setLayoutData(fillHorizontal);
        new Label(shell, SWT.NONE).setText("Konto bankowe");
        Text bank_account = new Text(shell, SWT.BORDER);
        bank_account.setLayoutData(fillHorizontal);
        Button submit = new Button(shell, SWT.NONE);
        submit.setText("Zapisz");

        if(!insert) {
            name.setText(data.getName());
            street.setText(data.getStreet());
            city.setText(data.getCity());
            zip.setText(data.getZip());
            tax_id.setText(String.valueOf(data.getTax_id()));
            bank_account.setText(data.getBank_account());
        } else data = new Company();

        submit.addListener(SWT.Selection, event -> {
            data.setName(name.getText())
                    .setStreet(street.getText())
                    .setCity(city.getText())
                    .setZip(zip.getText())
                    .setTax_id(Long.parseLong(tax_id.getText()))
                    .setBank_account(bank_account.getText());

            if(insert ? data.insert() > 0 : data.update()) {
                if(insert)
                    callback.addItem(data);
                else
                    callback.editItem(data);
                shell.close();
            } else ErrorMessage.open(shell);

        });


        shell.open();
    }

    interface ActionCallback {
        void addItem(Company company);
        void editItem(Company company);
    }
}