package InvoiceCreator.View.Invoice;

import InvoiceCreator.Controller.Company;
import InvoiceCreator.View.ErrorMessage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.*;

import java.time.LocalDate;
import java.util.HashMap;

import static org.eclipse.swt.layout.GridData.FILL_HORIZONTAL;

public class Invoice implements Product.ActionCallback {

    private Shell shell;
    private Table table;

    public void open() {
        Display display = new Display();
        shell = new Shell(display);
        shell.setText("Faktura");
        shell.setMinimumSize(950,500);
        GridLayout gridLayoutTwoEqualColumns = new GridLayout(2, true);
        shell.setLayout(gridLayoutTwoEqualColumns);
        new Composite(shell, SWT.NONE);
        Composite info = new Composite(shell, SWT.NONE);
        info.setLayout(gridLayoutTwoEqualColumns);
        new Label(info, SWT.NONE).setText("Faktura numer");
        Text number = new Text(info, SWT.BORDER);
        new Label(info, SWT.NONE).setText("Data wystawienia");
        DateTime date = new DateTime(info, SWT.BORDER);

        GridLayout gridLayoutTwoDifferentColumns = new GridLayout(2, false);
        GridData gridDataFillHorizontal = new GridData(SWT.FILL, SWT.CENTER, true, false);

        Group sellerGroup = new Group(shell, SWT.NONE);
        sellerGroup.setText("Sprzedający");
        sellerGroup.setLayout(gridLayoutTwoDifferentColumns);
        sellerGroup.setLayoutData(gridDataFillHorizontal);

        Group buyerGroup = new Group(shell, SWT.NONE);
        buyerGroup.setText("Kupujący");
        buyerGroup.setLayout(gridLayoutTwoDifferentColumns);
        buyerGroup.setLayoutData(gridDataFillHorizontal);

        new Label(sellerGroup, SWT.NONE).setText("Nazwa");

        Combo sellerName = new Combo(sellerGroup, SWT.MULTI);
        sellerName.setLayoutData(gridDataFillHorizontal);
        fillName(sellerName);

        new Label(buyerGroup, SWT.NONE).setText("Nazwa");

        Combo buyerName = new Combo(buyerGroup, SWT.NONE);
        buyerName.setLayoutData(gridDataFillHorizontal);
        fillName(buyerName);

        HashMap<String, Text> sellerData = new HashMap<>();
        companyDetailsTexts(sellerGroup, sellerData, gridDataFillHorizontal);

        HashMap<String, Text> buyerData = new HashMap<>();
        companyDetailsTexts(buyerGroup, buyerData, gridDataFillHorizontal);

        GridData fullWidthMergeTwoColumns = new GridData(FILL_HORIZONTAL);
        fullWidthMergeTwoColumns.grabExcessHorizontalSpace = true;
        fullWidthMergeTwoColumns.horizontalSpan = 2;

        Composite details = new Composite(shell, SWT.NONE);
        details.setLayout(gridLayoutTwoDifferentColumns);
        details.setLayoutData(fullWidthMergeTwoColumns);

        new Label(details, SWT.NONE).setText("Numer konta");
        sellerData.put("bank_account", new Text(details, SWT.BORDER));
        sellerData.get("bank_account").setTextLimit(26);
        sellerData.get("bank_account").setLayoutData(new GridData(FILL_HORIZONTAL));

        new Label(details, SWT.NONE).setText("Termin");
        DateTime paymentDate = new DateTime(details, SWT.NONE);
        LocalDate pickedPaymentDate = LocalDate.now().plusDays(14);
        paymentDate.setDate(pickedPaymentDate.getYear(), pickedPaymentDate.getMonthValue()-1, pickedPaymentDate.getDayOfMonth());

        new Label(details, SWT.NONE).setText("Waluta");
        Combo currency = new Combo(details, SWT.NONE);
        currency.setItems("PLN", "EUR", "GPB", "USD", "CHF");
        currency.select(0);

        GridData fillBothMergeColumns = new GridData(GridData.FILL_BOTH);
        fillBothMergeColumns.grabExcessHorizontalSpace = true;
        fillBothMergeColumns.horizontalSpan = 2;

        table = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        table.setLayoutData(fillBothMergeColumns);

        for (String title : new String[]{"Nazwa towaru/usługi", "Ilość", "Cena jednost.", "Wartość netto", "VAT %", "Kwota VAT", "Wartość brutto"}) {
            TableColumn column = new TableColumn(table, SWT.NONE);
            column.setText(title);
            column.pack();
        }

        Composite buttons = new Composite(shell, SWT.NONE);
        buttons.setLayout(new RowLayout());
        Button addProduct = new Button(buttons, SWT.NONE);
        addProduct.setText("Dodaj produkt");
        Button deleteProduct = new Button(buttons, SWT.NONE);
        deleteProduct.setText("Usuń produkt");
        Button manageCompanies = new Button(buttons, SWT.NONE);
        manageCompanies.setText("Podmioty");
        Button manageProducts = new Button(buttons, SWT.NONE);
        manageProducts.setText("Produkty");
        Button invoicesList = new Button(buttons, SWT.NONE);
        invoicesList.setText("Faktury");
        Button generate = new Button(buttons, SWT.NONE);
        generate.setText("Generuj fakturę");

        companyNameFill(sellerName);
        companyNameFill(buyerName);
        fillInputs(sellerName, sellerData);
        fillInputs(buyerName, buyerData);

        addProduct.addListener(SWT.Selection, event -> new Product(shell, Invoice.this).open());

        deleteProduct.addListener(SWT.Selection, event -> {
            if (table.getSelectionIndex() >= 0)
                table.getItem(table.getSelectionIndex()).dispose();
        });

        manageCompanies.addListener(SWT.Selection, event -> new InvoiceCreator.View.Company.List(shell).open());

        manageProducts.addListener(SWT.Selection, event -> new InvoiceCreator.View.Product.List(shell).open());

        invoicesList.addListener(SWT.Selection, event -> new List(shell).open());

        generate.addListener(SWT.Selection, event -> {
            try {
                InvoiceCreator.Model.Invoice invoice = new InvoiceCreator.Model.Invoice()
                    .setBuyer((InvoiceCreator.Model.Company) buyerName.getData())
                    .setSeller((InvoiceCreator.Model.Company) sellerName.getData())
                    .setCurrency(currency.getText())
                    .setNumber(number.getText())
                    .setDate(LocalDate.of(date.getYear(), date.getMonth(), date.getDay()))
                    .setPayment_date(LocalDate.of(paymentDate.getYear(), paymentDate.getMonth(), paymentDate.getDay()))
                    .setCurrency(currency.getText());
                for(TableItem item : table.getItems())
                    invoice.addProduct((InvoiceCreator.Model.Product) item.getData(), Integer.parseInt(item.getText(1)));
                InvoiceCreator.Controller.Invoice.generate(invoice);
            } catch (java.lang.Exception e) {
                e.printStackTrace();
                ErrorMessage.open(shell);
            }
        });

        shell.open();
        while (!shell.isDisposed())
            if (!display.readAndDispatch())
                display.sleep();
        display.dispose();
    }

    private void companyDetailsTexts(Group group, HashMap<String, Text> sellerData, GridData gridData) {
        for (java.util.Map.Entry<String, String> entry : java.util.Map.of(
                "street", "Ulica",
                "city", "Miasto",
                "zip", "Kod pocztowy",
                "tax_id", "NIP"
        ).entrySet()) {
            new Label(group, SWT.NONE).setText(entry.getValue());
            sellerData.put(entry.getKey(), new Text(group, SWT.BORDER));
            sellerData.get(entry.getKey()).setLayoutData(gridData);
        }
    }

    private void companyNameFill(Combo nameField) {
        nameField.addListener(SWT.KeyUp, event -> fillName(nameField));
    }

    private void fillName(Combo field) {
        String entered = field.getText();
        java.util.List<InvoiceCreator.Model.Company> companies = Company.companies(entered);
        field.removeAll();
        field.setItems(companies.stream().map(InvoiceCreator.Model.Company::getName).toArray(String[]::new));
        field.setListVisible(true);
        field.setText(entered);
        field.setSelection(new Point(entered.length(), entered.length()));
    }

    private void fillData(Combo nameField, HashMap<String, Text> fields) {
        InvoiceCreator.Model.Company company = Company.company(nameField.getText());
        if(company == null)
            return;
        nameField.setData(company);
        fields.get("street").setText(company.getStreet());
        fields.get("city").setText(company.getCity());
        fields.get("zip").setText(company.getZip());
        if(fields.get("bank_account") != null)
            fields.get("bank_account").setText(company.getBank_account());
        fields.get("tax_id").setText(String.valueOf(company.getTax_id()));
    }

    private void fillInputs(Combo nameField, HashMap<String, Text> fields) {
        nameField.addListener(SWT.FocusOut, event -> fillData(nameField, fields));
        nameField.addListener(SWT.Selection, event -> fillData(nameField, fields));
    }

    @Override
    public void addItem(InvoiceCreator.Model.Product product, int quantity) {
        java.text.DecimalFormat df=new java.text.DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        Double netto = product.getPrice() * quantity;
        Double taxAmount = netto* product.getTax() /100;
        TableItem item = new TableItem(table, SWT.NONE);
        item.setData(product);
        String[] columns = { product.getName(), String.valueOf(quantity), df.format(Double.valueOf(product.getPrice())), df.format(netto), df.format(product.getTax()), df.format(taxAmount), df.format(taxAmount+netto) };
        for(int i = 0; i<columns.length; i++)
            item.setText (i, columns[i]);
        for(TableColumn column : table.getColumns())
            column.pack();
    }
}
