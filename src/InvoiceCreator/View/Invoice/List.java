package InvoiceCreator.View.Invoice;

import InvoiceCreator.Model.BriefInvoice;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.*;

class List {

    private Shell parent;

    List(Shell parent) {
        this.parent = parent;
    }

    public void open() {
        Shell shell = new Shell(parent);
        shell.setLayout(new GridLayout(1,true));
        shell.setText("Faktury");
        shell.setSize(480, 320);

        Table table = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        for(String title : new String[]{ "Data", "Numer faktury", "Sprzedający", "Kupujący"})
            new TableColumn(table, SWT.NONE).setText(title);

        for (BriefInvoice bi : BriefInvoice.list()) {
            TableItem item = new TableItem(table, SWT.NONE);
            String[] columns = {bi.getDate().toString(), bi.getNumber(), bi.getSeller(), bi.getBuyer()};
            for(int i = 0; i<columns.length; i++)
                item.setText(i, columns[i]);
            item.setData(bi);
        }

        for(TableColumn column : table.getColumns())
            column.pack();

        Composite buttons = new Composite(shell, SWT.NONE);
        buttons.setLayout(new RowLayout());
        Button show = new Button(buttons, SWT.NONE);
        show.setText("Wyświetl");
        Button delete = new Button(buttons, SWT.NONE);
        delete.setText("Usuń");

        show.addListener(SWT.Selection, event -> {
            if(table.getSelectionIndex()>=0)
            try {
                BriefInvoice bi = (BriefInvoice) table.getItem(table.getSelectionIndex()).getData();
                new Generator(bi.get()).createInvoice();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        delete.addListener(SWT.Selection, event -> {
            if(table.getSelectionIndex()>=0) {
                TableItem item = table.getItem(table.getSelectionIndex());
                BriefInvoice bi = (BriefInvoice) item.getData();
                if(bi.delete())
                    item.dispose();
            }
        });

        shell.open();
    }

}
