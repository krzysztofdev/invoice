package InvoiceCreator.View.Invoice;

import InvoiceCreator.View.ErrorMessage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

class Product {

    private Shell parent;
    private Combo name;
    private Text price, tax;
    private ActionCallback callback;

    Product(Shell parent, ActionCallback callback) {
        this.parent = parent;
        this.callback = callback;
    }

    public void open() {
        Shell shell = new Shell(parent);
        shell.setText("Dodaj produkt do listy");
        shell.setSize(300, 180);
        shell.setLayout(new GridLayout(2, false));

        new Label(shell, SWT.NONE).setText("Nazwa");
        name = new Combo(shell, SWT.BORDER);
        name.setItems(InvoiceCreator.Controller.Product.products().stream().map(InvoiceCreator.Model.Product::getName).toArray(String[]::new));
        GridData layoutDataFillHorizontal = new GridData(GridData.FILL_HORIZONTAL);
        name.setLayoutData(layoutDataFillHorizontal);

        new Label(shell, SWT.NONE).setText("Ilość");
        Text quantity = new Text(shell, SWT.BORDER);
        quantity.setLayoutData(layoutDataFillHorizontal);

        new Label(shell, SWT.NONE).setText("Cena/szt");
        price = new Text(shell, SWT.BORDER);
        price.setLayoutData(layoutDataFillHorizontal);

        new Label(shell, SWT.NONE).setText("VAT %");
        tax = new Text(shell, SWT.BORDER);
        tax.setLayoutData(layoutDataFillHorizontal);

        Button add = new Button(shell, SWT.NONE);
        add.setText("Dodaj produkt");

        name.addListener(SWT.KeyUp, event -> {
            String entered = name.getText();
            java.util.List<InvoiceCreator.Model.Product> products = InvoiceCreator.Controller.Product.products(entered);
            name.removeAll();
            name.setItems(products.stream().map(InvoiceCreator.Model.Product::getName).toArray(String[]::new));
            name.setListVisible(true);
            name.setText(entered);
            name.setSelection(new Point(entered.length(), entered.length()));
        });
        name.addListener(SWT.FocusOut, event -> autoComplete());
        name.addListener(SWT.Selection, event -> autoComplete());

        add.addListener(SWT.Selection, event -> {
            InvoiceCreator.Model.Product product;
            try {
                 product = InvoiceCreator.Controller.Product.product(name.getText());
            } catch (Exception e) {
                e.printStackTrace();
                ErrorMessage.open(shell);
                return;
            }
            callback.addItem(product, Integer.parseInt(quantity.getText()));
            shell.close();
        });

        shell.open();
    }

    private void autoComplete() {
        InvoiceCreator.Model.Product product = InvoiceCreator.Controller.Product.product(name.getText());
        if (product != null) {
            price.setText(String.valueOf(product.getPrice()));
            tax.setText(String.valueOf(product.getTax()));
        }
    }

    interface ActionCallback {
        void addItem(InvoiceCreator.Model.Product product, int quantity);
    }

}
