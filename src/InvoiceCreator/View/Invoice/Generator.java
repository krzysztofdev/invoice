package InvoiceCreator.View.Invoice;

import InvoiceCreator.Model.Company;
import InvoiceCreator.Model.Invoice;
import InvoiceCreator.Model.Product;

import java.awt.*;
import java.io.*;
import java.util.Map;
import java.util.Scanner;

public class Generator {

    private Invoice invoice;

    public Generator(Invoice invoice) {
        this.invoice = invoice;
    }

    public void createInvoice() throws IOException {
        Company seller = invoice.getSeller();
        Company buyer = invoice.getBuyer();
        StringBuilder template = new StringBuilder();

        Scanner in = new Scanner(new File("template.html"));
        while(in.hasNextLine())
            template.append(in.nextLine());
        in.close();

        java.text.DecimalFormat df=new java.text.DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        StringBuilder table = new StringBuilder();

        int i = 1;
        double nettoSum = 0;
        double taxSum = 0;
        double bruttoSum = 0;
        for(Map.Entry<Product, Integer> entry : invoice.getProducts().entrySet()) {
            double netto = entry.getKey().getPrice()*entry.getValue();
            double taxValue = entry.getKey().getPrice() * entry.getValue() * entry.getKey().getTax() / 100;
            table.append("<tr><td class=\"lp\">").append(i).append("</td>");
            table.append("<td class=\"nazwa\">").append(entry.getKey().getName()).append("</td>");
            table.append("<td class=\"ilosc\">").append(entry.getValue()).append("</td>");
            table.append("<td class=\"cena\">").append(df.format(entry.getKey().getPrice())).append("</td>");
            table.append("<td class=\"netto\">").append(df.format(netto)).append("</td>");
            table.append("<td class=\"vat\">").append(df.format(entry.getKey().getTax())).append("</td>");
            table.append("<td class=\"vatn\">").append(df.format(taxValue)).append("</td>");
            table.append("<td class=\"brutto\">").append(df.format(taxValue + netto)).append("</td></tr>");
            nettoSum += netto;
            taxSum += taxValue;
            bruttoSum += taxValue+netto;
            i++;
        }
        table.append("<tr><td></td><td class=\"b\">Razem:</td><td></td><td></td><td class=\"b\">").append(df.format(nettoSum)).append("</td><td></td><td class=\"b\">").append(df.format(taxSum)).append("</td><td class=\"b\">").append(df.format(bruttoSum)).append("</td></tr>");

        String completed = template.toString()
                .replace("{sName}", seller.getName())
                .replace("{sStreet}", seller.getStreet())
                .replace("{sCity}", seller.getCity())
                .replace("{sZip}", seller.getZip())
                .replace("{sTax_id}", String.valueOf(seller.getTax_id()))
                .replace("{sBank_account}", seller.getBank_account())
                .replace("{bName}", buyer.getName())
                .replace("{bStreet}", buyer.getStreet())
                .replace("{bCity}", buyer.getCity())
                .replace("{bZip}", buyer.getZip())
                .replace("{bTax_id}", String.valueOf(buyer.getTax_id()))
                .replace("{currency}", invoice.getCurrency())
                .replace("{invoiceNumber}", invoice.getNumber())
                .replace("{date}", invoice.getDate().toString())
                .replace("{paymentDate}", invoice.getPayment_date().toString())
                .replace("{table}", table.toString());

        PrintWriter out = new PrintWriter("invoice.html");
        out.println(completed);
        out.close();

        Desktop.getDesktop().open(new File("invoice.html"));
    }

}
